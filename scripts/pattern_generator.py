#!/usr/bin/env python

# This script generates the pattern files used to import the data
# The top directory containing the folder with images
# should be specified.

import datetime
import glob
import os
import re
import shutil
import sys

base_path = "../"
generation_path = "/tmp/"

def find_range(value):
    """
    Find the t and c range
    """
    m = re.search('_T(.+?)F', value)
    if not m:
        return -1
    return m.group(1)


def generate_pattern_file(directory):
    """
    Walk through the various directories and generate
    a pattern file in each directory containing the *.tif images
    """
    date = str(datetime.date.today()).replace("-", "")
    print(os.path.abspath(os.path.join(directory, os.pardir)))
    generation_path = os.path.abspath(os.path.join(directory, os.pardir))
    base_path = generation_path
    pattern_location = os.path.join(generation_path, date + "-pattern")
    if (os.path.exists(pattern_location)):
        shutil.rmtree(pattern_location)
    os.mkdir(pattern_location, 0o755)
    for subdir, dirs, files in os.walk(directory):
        path, name = os.path.split(subdir)
        pattern = os.path.join(subdir, "*.tif")
        fnames = sorted(glob.glob(pattern))
        if len(fnames) == 0:
            continue
        basename = os.path.basename(fnames[0])
        name, extension = os.path.splitext(basename)
        names = basename.split("_T")
        name = names[0]
        file_name = name + ".pattern"
        t1 = find_range(fnames[0])
        if t1 == -1:
            continue
        t2 = find_range(fnames[-1])
        if t2 == -1:
            continue
        v = name + "_T<%s-%s>%s" % (t1, t2, names[1].replace(t1, ""))
        value = v
        print("creating pattern file: %s" % file_name)
        with open(os.path.join(pattern_location, file_name), "w") as f:
            f.write(value)


def main(argv):
    if len(argv) == 0:
        directory = os.getcwd()
    else:
        directory = argv[0]
    generate_pattern_file(directory)


if __name__ == "__main__":
    main(sys.argv[1:])
