#!/usr/bin/env python

# -----------------------------------------------------------------------------
#   Copyright (C) 2020 University of Dundee. All rights reserved.

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# ------------------------------------------------------------------------------

# This script converts the output of trackpy saved as txt.
# Each line is read and converted into ROIs in OMERO


import argparse
import getpass
import os
from pathlib import Path
import re
import sys
import tempfile

# omero
from omero.gateway import BlitzGateway
from omero.model import PointI, PolylineI, RoiI
from omero.rtypes import rdouble, rint, rstring


import pandas as pd

trackpy_version = "0.4.2"


def rgba_to_int(red, green, blue, alpha=255):
    return int.from_bytes([red, green, blue, alpha], byteorder='big',
                          signed=True)


def save_tracks(conn, image, tracks, groupId):
    """
    Save the tracks back to OMERO
    """
    columns = list(tracks.columns)
    columns.append("RoiID")
    columns.append("ImageID")
    general_df = pd.DataFrame(columns=columns)

    svc = conn.getUpdateService()
    df = tracks['particle'].unique().tolist()
    for c in df:
        track = tracks.loc[tracks['particle'] == c]
        roi = RoiI()
        roi.setImage(image._obj)
        points = ""
        for index, row in track.iterrows():
            # if the radius changes, we should use ellipse
            # (point is for radius 5)
            point = PointI()
            point.x = rdouble(float(row['x']))
            point.y = rdouble(float(row['y']))
            point.theT = rint(int(row['frame']))
            roi.addShape(point)
            points += "%s,%s " % (str(row['x']), str(row['y']))
        polyline = PolylineI()
        polyline.points = rstring(points)
        polyline.fillColor = rint(rgba_to_int(255, 0, 255, 50))
        roi.addShape(polyline)
        if points:
            ctx = {'omero.group':str(groupId)}
            roi = svc.saveAndReturnObject(roi, ctx)
            # Set the ROI Id in the data frame
            track = track.copy()
            track['RoiID'] = roi.getId().getValue()
            general_df = general_df.copy().append(track, ignore_index=True)

    general_df['ImageID'] = image.getId()
    return general_df


def save_results(conn, image, data, file_name):
    """
    Save data frame as CSV and link it to the image.
    """
    archive_name = "omero_%s.csv" % file_name
    tmp = os.path.join(tempfile.gettempdir(), archive_name)
    data.to_csv(path_or_buf=tmp, index=False)
    namespace = "trackpy_version_%s" % trackpy_version
    file_ann = conn.createFileAnnfromLocalFile(tmp,
                                               mimetype="text/csv",
                                               ns=namespace, desc=None)
    image.linkAnnotation(file_ann)
    os.remove(tmp)


def prepare_name(file_name):
    file_name = file_name.replace(".csv", "")
    values = file_name.split("_")
    original_name = ""
    n = len(values)-2
    for i in range(n):
        original_name += values[i]
        if i < (n - 1):
            original_name += "_"
    original_name += ".wpi"
    field = values[len(values)-1].replace("p", "Field ")
    ref_well = values[len(values)-2]
    letter = ''.join([i for i in ref_well if not i.isdigit()])
    wn = re.findall(r'\d+', ref_well)
    v = int(wn[0])
    well = "[Well %s%s," % (letter, v)
    return "%s %s %s]" % (original_name, well, field)


def parse_dir(conn, directory):
    query_svc = conn.getQueryService()
    for subdir, dirs, files in os.walk(directory):
        for f in Path(subdir).glob('*.csv'):
            file_name = prepare_name(Path(f).stem)
            # Find the image in omero.
            name = file_name + "%"
            query = "select i from Image i where i.name like '%s'" % name
            print(name)
            ctx = {'omero.group': '-1'}
            images = query_svc.findAllByQuery(query, None, ctx)
            if len(images) == 0:
                print("no image found: %s" % file_name)
                continue
            # convert into dataframes
            tracks = pd.read_csv(f)
            for index in range(len(images)):
                print("converting into rois...")
                groupId = images[index].getDetails().getGroup().getId().getValue()
                conn.SERVICE_OPTS.setOmeroGroup(groupId)
                image = conn.getObject("Image", images[index].getId())
                data = save_tracks(conn, image, tracks, groupId)
                save_results(conn, image, data, Path(f).stem)


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('directory',
                        help="The directory to scan. Check for .csv file")
    parser.add_argument('--username', default="trainer-1", help="The user to connect as")
    parser.add_argument('--server', default="outreach.openmicroscopy.org", 
                        help="The server to connect to")
    parser.add_argument('--port', default=4064)
    args = parser.parse_args(args)
    password = getpass.getpass()
    # Load the image corresponding to the file name
    # Create a connection
    try:
        conn = BlitzGateway(args.username, password, host=args.server,
                            port=args.port)
        conn.connect()
        conn.c.enableKeepAlive(60)
        print("Parsing folder...")
        parse_dir(conn, args.directory)
    finally:
        conn.close()
        print("done")



if __name__ == "__main__":
    main(sys.argv[1:])